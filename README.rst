
Steve Allen's Contributions
===========================

This project provides an insight to my main contributions to various scientific fields including high performance computing, nanotechnology, artificial Intelligence and physics

List of Documents
-----------------

* codes.rst: List of algorithm and coding contributions
* conferences.rst: List of conference I've given. 
* classes.rst: List of classes taught and hands-on directed. 
* papers.rst: List of published papers. 
* wikis.rst: List of documentation I contributed in. 
* README.rst: Project content overview and major contribution highlights

Codes
-----

* **Reduced Liouvillian Transformation** (in progress)
* **Generic Code**: Tools to build an algorithm and data pipeline `Bitbucket <https://bitbucket.org/allenste/genericcode>`_
* **Watershed**: Image Analysis `Bitbucket <https://bitbucket.org/allenste/watershed>`_
* **Extended cubic spline**: Fit and modelization `Bitbucket <https://bitbucket.org/allenste/spline-maximum-entropy>`_
* **ARMA filter for unevenly sampled data** `Bitbucket <https://bitbucket.org/allenste/arma-filter>`_
* **Maximum Entropy**: To solve an ill-posed inverse problem
* **Sherbrooke Quantum Package** (SQUACK): Quantum Physics Calculation tool (private) `SourceForge <https://sourceforge.net/projects/squack/>`_
* **Hubbard**: Solving the Hubbard model by Markov Chain Quantum Monte Carlo

Please visit my complete list of codes in the file codes.rst in this repository.

Conferences
-----------

* Parallel programming with Python: `Apius 2013 <http://apius.ca/>`_ 
* Tutorial on Parallel Programming (2006, 2008, 2011)
* Numerical Continuation of Analytical Function `HPCS <http://hpcs2003.ccs.usherbrooke.ca/program.html>`_
* Hydrous Area Segmentation in Radar Imagery `MITACS <http://www.crm.umontreal.ca/pub/Rapports/2700-2799/2746.pdf>`_
* Signal Based Features `Fusion <http://www.isif.org/fusion/proceedings/fusion01CD/fusion/searchengine/pdf/FrC21.pdf>`_
* Conserving Approximations vs Two-Particle Self-Consistent Approach `CRM <https://www.physique.usherbrooke.ca/pages/sites/default/files/Theoretical_Methods_for_Strongly_Correlated_Electrons(pages_frontispices).pdf>`_

Papers
------

* Two-particle self-consistent approximation, pseudogap and superconductivity `Thesis <http://arxiv.org/abs/cond-mat/0012301>`_
* Non-perturbative approach to the attractive Hubbard model `PRB <http://arxiv.org/abs/cond-mat/0010039>`_
* Pairing fluctuations and pseudogaps in the attractive Hubbard model `PRB <http://arxiv.org/abs/cond-mat/0010001>`_
* Many-body Theory vs Simulations for the pseudogap in the Hubbard model `PRB <http://arxiv.org/pdf/cond-mat/9908053.pdf>`_
* Role of symmetry and dimension on pseudogap phenomena `PRL <http://arxiv.org/abs/cond-mat/9904436>`_
* Attractive Hubbard model and single-particle pseudogap due to classical pairing fluctuations `J.Phys.Chem.Solids <http://arxiv.org/abs/cond-mat/9710013>`_
* The Peierls Transition in a Magnetic Field `Europhysics <http://iopscience.iop.org/0295-5075/32/8/008>`_

Classes
-------

* Parallel Programming, hands-on (2012)
* Introduction to using a cluster, hands-on (2012)
* Quantum Mechanics III (2003, 2006)
* Quantum Statistical Mechanics (2005)
* Statistical Physics I (2003)

Other Projects
--------------

* https://wiki.calculquebec.ca/

