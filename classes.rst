
Classes and Tutorials
=====================

High Performance Computing
--------------------------

* Parallel Programming, hands-on (2012)
* Introduction to using a cluster, hands-on (2012)

Physics
-------

Master Degree Level:

* Quantum Statistical Mechanics (2005)

Baccaluareate Level:

* Quantum Mechanics III (2003, 2006)
* Statistical Physics I (2003)
