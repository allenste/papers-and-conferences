================
Scientific Codes
================

Most of these projects can be on obtained on request to
steve.allen@usherbrooke.ca. Only a few of them are opened.

General Mathematical Algorithms
-------------------------------

**Reduced Liouvillian Transformation** : In progress
    Python, Numpy, scipy.sparse, f2py, ctypes, C, C++ (Squack)

    Implemented to be used with QuTiP and SQUACK

**Generic Code** : `Bitbucket <https://bitbucket.org/allenste/genericcode>`_
    Python, Mpi, HDF5, Json, Pickle, Metaprogramming

    Tools to build an algorithm and data pipeline 

**Extended cubic spline** : `Bitbucket <https://bitbucket.org/allenste/extended-cubic-spline>`_
    cubic splines, Python, ctypes

    Produce a infinitely wide grid to modelize a 1D function. Ideal to modelize a function in an inverse problem.

**Maximum Entropy** : `wiki <https://bitbucket.org/allenste/spline-maximum-entropy.git/wiki>`_
    C++, OpenMp, entropy, Bayes analysis

    Numerical continuation of an analytical function in the complex plane by Maximum entropy (to regularize the problem) and the use of Bayesian analysis.
    Code available on demand.


Imagery
-------

The next five project were developed with Biomechanic laboratory at Universite
de Sherbrooke. The code might be available on request. To see a short
description visit de following `wiki <https://bitbucket.org/allenste/fuzzy-distance-transform.git/wiki>`_

**Watershed** : On request
    Python, MPI, Numpy, HDF5, random permutation, multidimensional slicing

**FDT** : Private
    C++, MPI

    Fuzzy distance transform of a set of images.

**Resorption** : Private
    Python, ctypes, Numpy, C, MPI, OpenMp, SSE

    Perform the resorption of a 3D set of images.

**Image Reconstruction** : Private
    C++, MPI

    Using a skeleton and the FDT images, it rebuild a 3D set of images.

**Skeletonization** : Private
    C++, MPI

    Use the FDT of a set of images to find the skeleton of the object in the image.

**Level Set-based Snakes** : Private
    C++

    Find the contour of objects in images with various topology. The topology does not need to be known.


Artificial Intelligence
-----------------------

**Ship Recognition in FLIR images** : private
    C++

    Use a classification tree to classify ships in FLIR images in 8 categories. Tested with set of images taken at 30 and 90 degrees.
    

Electrical Engineering
----------------------

**ARMA filter for unevenly sampled data** : `Bitbucket <https://bitbucket.org/allenste/arma-filter.git>`_
    Python, Numpy, Matplotlib, Tkinter, MPI, Lomb

    This is a filter for a large set of unevenly sampled data. It builds an autoregressive model and then execute an ARMA filter. Can be executed in batch or interactively with a GUI.


Physics
-------

**Sherbrooke Quantum Package** : (private) `SourceForge <https://sourceforge.net/projects/squack/>`_
    C++, OpenMp, Sparce matrices, jaggered diagonal matrix

    This project uses very fast matricial operation between diagonal matrices and full matrices. The written library is much faster than anything else on the market (e.g., MKL)

**Hubbard** : On request
    Fortran 90, MPI.

    Markov Chain Quantum Monte Carlo (Determinental method).

**Random Walker** : On request `Wiki <https://bitbucket.org/allenste/random-walker.git/wiki>`_
    C++

    Markov chain Monte Carlo used to estimate the porosity of a media. The media is represented by a triangulation method.


Geophysics
----------

**Snow modelisation** : On request
    C, MPI

    Uses the equation of heat propagation in snow with land topology and data from the weather centers to simulate snow depth and its water content.

**Land Use Mapping** : private, `ICCRTS <http://www.dodccrp.org/events/7th_ICCRTS/Tracks/pdf/018.PDF>`_
    C++

    Builds a mapping of the land use based on multispectral imagery.
