
Conferences
===========

High Performance Computing
--------------------------

* Parallel programming with Python: `Apius 2013 <http://apius.ca/>`_ 
* Tutorial on Parallel Programming (2006, 2008, 2011)
* Numerical Continuation of Analytical Function `HPCS <http://hpcs2003.ccs.usherbrooke.ca/program.html>`_

Imagery
-------

* Hydrous Area Segmentation in Radar Imagery `MITACS <http://www.crm.umontreal.ca/pub/Rapports/2700-2799/2746.pdf>`_
* Signal Based Features with Application to Ship Recognition `Fusion <http://www.isif.org/fusion/proceedings/fusion01CD/fusion/searchengine/pdf/FrC21.pdf>`_

Physics
-------

* Conserving Approximations vs Two-Particle Self-Consistent Approach `CRM <https://www.physique.usherbrooke.ca/pages/sites/default/files/Theoretical_Methods_for_Strongly_Correlated_Electrons(pages_frontispices).pdf>`_

Presented by Colleagues
-----------------------

* Analysis of aerosol optical statistics within and above the PBL
* Design, Deployment and Bench of a Large Infiniband HPC Cluster `HPCS <http://doi.ieeecomputersociety.org/10.1109/HPCS.2006.18>`_
* Land Use Mapping using Evidential Fusion `ICCRTS <http://www.dodccrp.org/events/7th_ICCRTS/Tracks/pdf/018.PDF>`_
