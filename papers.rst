
Papers Reviewed by Peers
========================

Physics
-------

* Two-particle self-consistent approximation, pseudogap and superconductivity `Thesis <http://arxiv.org/abs/cond-mat/0012301>`_
* Non-perturbative approach to the attractive Hubbard model `PRB <http://arxiv.org/abs/cond-mat/0010039>`_
* Pairing fluctuations and pseudogaps in the attractive Hubbard model `PRB <http://arxiv.org/abs/cond-mat/0010001>`_
* Many-body Theory vs Simulations for the pseudogap in the Hubbard model `PRB <http://arxiv.org/pdf/cond-mat/9908053.pdf>`_
* Role of symmetry and dimension on pseudogap phenomena `PRL <http://arxiv.org/abs/cond-mat/9904436>`_
* Attractive Hubbard model and single-particle pseudogap due to classical pairing fluctuations `J.Phys.Chem.Solids <http://arxiv.org/abs/cond-mat/9710013>`_
* The Peierls Transition in a Magnetic Field `Europhysics <http://iopscience.iop.org/0295-5075/32/8/008>`_
